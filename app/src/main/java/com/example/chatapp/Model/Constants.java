package com.example.chatapp.Model;

import java.util.HashMap;

public class Constants {
    public static final String KEY_COLLECTION_USERS = "Users";
    public static final String KEY_FULL_NAME= "fullname";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD="password";
    public static final String KEY_USER_ID = "userId";
    public static final String KEY_FCM_TOKEN = "token";

    public static final String KEY_PREFERENCE_NAME = "videoMeetingPreference";
    public static final String KEY_IS_SIGNED_IN = "isSignedIn";

    public static final String REMOTE_MSG_AUTHORIZATION = "Authorization";
    public static final String REMOTE_MSG_CONTENT_TYPE = "Content-Type";

    public static final String REMOTE_MSG_TYPE = "type";

    public static final String REMOTE_MSG_INVITATION = "invitation";

    public static final String REMOTE_MSG_MEETTING_TYPE = "meetingType";

    public static final String REMOTE_MSG_INVITER_TOKEN = "inviterToken";

    public static final String REMOTE_MSG_DATA = "data";

    public static final String REMOTE_MSG_REGISTRATION_IDS = "registration_ids";

    public static final String REMOTE_MSG_INVITATION_RESPONSE = "invitationResponse";

    public static final String REMOTE_MSG_INVITATION_ACCEPTED = "accepted";

    public static final String REMOTE_MSG_INVITATION_REJECTED = "rejected";

    public static final String REMOTE_MSG_INVITATION_CANCELLED = "cancelled";

    public static final String REMOTE_MSG_MEETING_ROOM = "meetingRoom";
    public static HashMap<String,String> getRemoteMessageHeader(){
        HashMap<String,String> headers = new HashMap<>();
        headers.put(Constants.REMOTE_MSG_AUTHORIZATION,
                "BGFg1aa6svq_qgNkE8VwdsEgOwy17s5sm4iPhrCqsuWzBgH_Nz6T8We4yLw1oqb8AwT15eryEkshtF9lnYYHfVk");
        headers.put(Constants.REMOTE_MSG_CONTENT_TYPE,"application/json");
        return headers;
    }
}
