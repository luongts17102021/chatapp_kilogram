package com.example.chatapp.Activity.VideoCall;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.chatapp.Model.Constants;
import com.example.chatapp.NetWork.ApiClient;
import com.example.chatapp.NetWork.ApiService;
import com.example.chatapp.R;

import org.jitsi.meet.sdk.JitsiMeetActivity;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IncomingInvitationActivity extends AppCompatActivity {

    private String meetingType = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incoming_invitation);

        ImageView imageMeetingTyoe = findViewById(R.id.imageMeetingType);
        meetingType = getIntent().getStringExtra(Constants.REMOTE_MSG_MEETTING_TYPE);

        if(meetingType != null){
            if(meetingType.equals("video")){
                imageMeetingTyoe.setImageResource(R.drawable.baseline_videocam_24);
            }else{
                imageMeetingTyoe.setImageResource(R.drawable.baseline_call_24);
            }
        }

        TextView userName = findViewById(R.id.textUserName);
        TextView mail = findViewById(R.id.textEmail);

        ImageView imageAcceptInvitation = findViewById(R.id.imageAcceptInvitation);
        imageAcceptInvitation.setOnClickListener(v -> sendInvitationRespon(
                Constants.REMOTE_MSG_INVITATION_ACCEPTED,getIntent()
                        .getStringExtra(Constants.REMOTE_MSG_INVITER_TOKEN)));

         ImageView imageRejectInvitation = findViewById(R.id.imageRejectInvitation);
         imageRejectInvitation.setOnClickListener(v -> sendInvitationRespon(
                 Constants.REMOTE_MSG_INVITATION_REJECTED,getIntent()
                         .getStringExtra(Constants.REMOTE_MSG_INVITER_TOKEN)));
    }
    public void sendInvitationRespon(String meetingType, String receiverToken){
        try {
            JSONArray tokens = new JSONArray();
            tokens.put(receiverToken);

            JSONObject body = new JSONObject();
            JSONObject data = new JSONObject();

            data.put(Constants.REMOTE_MSG_TYPE, Constants.REMOTE_MSG_INVITATION_RESPONSE);
            data.put(Constants.REMOTE_MSG_INVITATION_RESPONSE, meetingType);


            body.put(Constants.REMOTE_MSG_DATA,data);
            body.put(Constants.REMOTE_MSG_REGISTRATION_IDS, tokens);

            sendRemoteMessage(body.toString(), Constants.REMOTE_MSG_INVITATION);


        } catch (JSONException e) {
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void sendRemoteMessage(String remoteMessingBody, String type){

        ApiClient.getClient().create(ApiService.class).sendRemoteMessage(
                        Constants.getRemoteMessageHeader(),remoteMessingBody)
                   .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if(response.isSuccessful()){
                            if(type.equals(Constants.REMOTE_MSG_INVITATION_ACCEPTED)){
                                try {
                                    URL serverURL = new URL("http://meet.jit.si");
                                    JitsiMeetConferenceOptions.Builder builder = new JitsiMeetConferenceOptions.Builder();
                                    builder.setServerURL(serverURL);
                                    builder.setRoom(Constants.REMOTE_MSG_MEETING_ROOM);
                                    if(meetingType.equals("audio")){
                                        builder.setVideoMuted(true);
                                    }
                                    JitsiMeetActivity.launch(IncomingInvitationActivity.this,builder.build());
                                finish();
                                }catch (Exception exception){
                                    Toast.makeText(IncomingInvitationActivity.this,"Reject",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(IncomingInvitationActivity.this,"Reject",Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }else{
                            Toast.makeText(IncomingInvitationActivity.this,"",Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(IncomingInvitationActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
    }
    private BroadcastReceiver invitationResponseReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra(Constants.REMOTE_MSG_INVITATION_RESPONSE);
            if(type != null){
                if(type.equals(Constants.REMOTE_MSG_INVITATION_CANCELLED)){
                    try{

                    }catch (Exception exception){

                    }
                    finish();
                }else {
                    Toast.makeText(context,"",Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };

    @Override
    protected void onStart(){
        super.onStart();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(
                invitationResponseReceiver,
                new IntentFilter(Constants.REMOTE_MSG_INVITATION_CANCELLED)
        );
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(
                invitationResponseReceiver
        );
    }
}