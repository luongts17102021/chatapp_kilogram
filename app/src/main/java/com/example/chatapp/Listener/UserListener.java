package com.example.chatapp.Listener;

import com.example.chatapp.Model.Users;


public interface UserListener {


    void initateVideoMeeting(Users user);

    void initateAudioMeeting(Users user);

}
