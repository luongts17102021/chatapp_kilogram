package com.example.chatapp.Model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Users implements Serializable {
    String userId;
    String fullname;
    String mail;
    String password;
    String image;

    private String lastMessage;

    private String lastMessageTime;

    private String status;

    private String token;


    public Users() {
    }

    public Users(String userId, String fullname, String mail, String password, String image, String status) {
        this.userId = userId;
        this.fullname = fullname;
        this.mail = mail;
        this.password = password;
        this.image = image;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Users(String userId, String fullname, String mail, String password, String image, String lastMessage, String lastMessageTime, String status) {
        this.userId = userId;
        this.fullname = fullname;
        this.mail = mail;
        this.password = password;
        this.image = image;
        this.lastMessage = lastMessage;
        this.lastMessageTime = lastMessageTime;
        this.status = status;
    }

    public Users(String userId, String fullname, String mail, String password, String image, String lastMessage, String lastMessageTime) {
        this.userId = userId;
        this.fullname = fullname;
        this.mail = mail;
        this.password = password;
        this.image = image;
        this.lastMessage = lastMessage;
        this.lastMessageTime = lastMessageTime;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(String lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public Users(String fullname, String mail, String password) {
        this.fullname = fullname;
        this.mail = mail;
        this.password = password;
    }
    public Users(String userId, String fullname, String mail, String password) {
        this.userId = userId;
        this.fullname = fullname;
        this.mail = mail;
        this.password = password;
    }



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Users(String userId, String fullname, String mail, String password, String image) {
        this.userId = userId;
        this.fullname = fullname;
        this.mail = mail;
        this.password = password;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
