package com.example.chatapp.Fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.Adapter.UserAdapter;
import com.example.chatapp.Model.Chat;
import com.example.chatapp.Model.Users;
import com.example.chatapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ChatFragment extends Fragment {
    private RecyclerView recyclerView;
    private UserAdapter userAdapter;
    private List<Users> mUsers;
    private List<String> listUser;
    private Map<String, Chat> lastMessageMap = new HashMap<>();
    FirebaseUser fUser;
    DatabaseReference reference;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        recyclerView = view.findViewById(R.id.rec_view_chat);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));



        fUser = FirebaseAuth.getInstance().getCurrentUser();
        listUser = new ArrayList<>();

        reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                listUser.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    Chat chat = dataSnapshot.getValue(Chat.class);
                    if (chat.getSender().equals(fUser.getUid())) {
                        listUser.add(chat.getReceiver());
                    }
                    if (chat.getReceiver().equals(fUser.getUid())) {
                        listUser.add(chat.getSender());
                    }

                }
                readChat();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        return view;

    }

    private void readChat() {
        mUsers = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Set<String> userIds = new HashSet<>();
                for (String id : listUser) {
                    userIds.add(id);
                }
                List<Users> distinctUsers = new ArrayList<>();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    Users user = dataSnapshot.getValue(Users.class);
                    if (userIds.contains(user.getUserId())) {
                        distinctUsers.add(user);
                        userIds.remove(user.getUserId());
                    }
                }
                // Sort the distinctUsers list based on the last message sent
                Collections.sort(distinctUsers, (u1, u2) -> {
                    String time1 = u1.getLastMessageTime();
                    String time2 = u2.getLastMessageTime();
                    if (time1 == null && time2 == null) {
                        return 0;
                    } else if (time1 == null) {
                        return 1;
                    } else if (time2 == null) {
                        return -1;
                    } else {
                        return time2.compareTo(time1);
                    }
                });

                // Move users with new messages to the top of the list
                List<Users> newMessageUsers = new ArrayList<>();
                List<Users> oldMessageUsers = new ArrayList<>();
                for (Users user : distinctUsers) {
                    if (user.getLastMessageTime() != null && !user.getLastMessageTime().equals("")) {
                        newMessageUsers.add(user);
                    } else {
                        oldMessageUsers.add(user);
                    }
                }
                newMessageUsers.addAll(oldMessageUsers);

                userAdapter = new UserAdapter(getContext(), newMessageUsers,true);
                recyclerView.setAdapter(userAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}