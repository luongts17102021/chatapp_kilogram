package com.example.chatapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.chatapp.Activity.Chat.ChatActivity;
import com.example.chatapp.Listener.UserListener;
import com.example.chatapp.Model.Chat;
import com.example.chatapp.Model.Users;
import com.example.chatapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
    private Context mContext;
    private List<Users> mUsers;
    private String theLastMessage;
    private UserListener userListener;
    private boolean isChat;

    private boolean isOnline;

    public UserAdapter(Context mContext, List<Users> mUsers, boolean isOnline) {
        this.mContext = mContext;
        this.mUsers = mUsers;
        this.isOnline = isOnline;

    }

    public UserAdapter(List<Users> users, UserListener userListener, boolean isChat){
        this.mUsers = users;
        this.userListener = userListener;
        this.isChat = isChat;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.user_list, parent, false);

        return new UserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Users users = mUsers.get(position);
        holder.user.setText(users.getFullname());
        if (users.getImage() != null && !users.getImage().equalsIgnoreCase("default")) {
            Glide.with(mContext).load(users.getImage()).into(holder.image);
        } else {
            holder.image.setImageResource(R.mipmap.ic_launcher);
        }
        lastMessage(users.getUserId(), holder.last_msg);

        if (isOnline){
            if (users.getStatus().equalsIgnoreCase("online")){
                holder.imgOnline.setVisibility(View.VISIBLE);
                holder.imgOffline.setVisibility(View.GONE);
            }else{
                holder.imgOnline.setVisibility(View.GONE);
                holder.imgOffline.setVisibility(View.VISIBLE);
            }
        }else {
            holder.imgOnline.setVisibility(View.GONE);
            holder.imgOffline.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra("userid", users.getUserId());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView user,textUserName;
        public ImageView image;
        private ImageView imgOnline;
        private ImageView imgOffline;
        ImageView imageAudioMeeting, imageVideoMeeting;
        private TextView last_msg;




        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            user = itemView.findViewById(R.id.username);
            image = itemView.findViewById(R.id.profile_image);
            imgOnline = itemView.findViewById(R.id.img_on);
            imgOffline = itemView.findViewById(R.id.img_off);
            imageAudioMeeting = itemView.findViewById(R.id.imageAudioCall);
            imageVideoMeeting = itemView.findViewById(R.id.imageVideoCall);
            textUserName = itemView.findViewById(R.id.textUserName);
            last_msg = itemView.findViewById(R.id.last_msg);
        }

        void setUserData(Users user){
            textUserName.setText(user.getFullname());
            imageVideoMeeting.setOnClickListener(v -> userListener.initateVideoMeeting(user));

            imageAudioMeeting.setOnClickListener(v -> userListener.initateAudioMeeting(user));
        }
        // check last message


    }
    public   void lastMessage(String userid, TextView last_msg){
        theLastMessage = "default";
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Chat chat = dataSnapshot.getValue(Chat.class);
                    if (chat.getReceiver().equals(firebaseUser.getUid())&& chat.getSender().equals(userid)
                            || chat.getReceiver().equals(userid) && chat.getSender().equals(firebaseUser.getUid())){
                        theLastMessage = chat.getMessage();
                        if (chat.getReceiver().equals(firebaseUser.getUid()) && !chat.isIsSeen()){
                            // set the text to bold
                            last_msg.setText(Html.fromHtml("<b>" + theLastMessage + "</b>"));
                        } else {
                            last_msg.setText(theLastMessage);
                        }
                    }
                }
                switch (theLastMessage){
                    case "default":
                        last_msg.setText("");
                        break;
                }
                theLastMessage = "default";
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }}
}