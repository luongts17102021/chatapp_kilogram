package com.example.chatapp.Activity.Chat;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.chatapp.Activity.Main.MainActivity;
import com.example.chatapp.Listener.UserListener;
import com.example.chatapp.Activity.VideoCall.OutGoingInvititationActivity;
import com.example.chatapp.Adapter.MessageAdapter;
import com.example.chatapp.Model.Chat;
import com.example.chatapp.Model.Users;
import com.example.chatapp.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity implements UserListener {

    private CircleImageView profile_image;
    private TextView username;
    private FirebaseUser fUser;
    private DatabaseReference reference;
    private Intent intent;

    ValueEventListener seenListener;

    private ImageButton btnSend;

    private ImageView videoCall;

    private ImageView audioCall;
    EditText edtTextSend;
    private Toolbar toolbar;

    MessageAdapter messageAdapter;
    List<Chat> mChat;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        videoCall = findViewById(R.id.imageVideoCall);
        audioCall = findViewById(R.id.imageAudioCall);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> startActivity(new Intent(ChatActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));

        recyclerView = findViewById(R.id.recycleView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        profile_image = findViewById(R.id.profile_image);
        username = findViewById(R.id.username);
        btnSend = findViewById(R.id.btn_send);
        edtTextSend = findViewById(R.id.text_send);

        intent = getIntent();
        String userid = intent.getStringExtra("userid");

        btnSend.setOnClickListener(v -> {
            String msg = edtTextSend.getText().toString();
            if (!msg.equals("")) {
                sendMessage(fUser.getUid(), userid, msg);
            } else {
                Toast.makeText(ChatActivity.this, "You cant send empty message", Toast.LENGTH_SHORT).show();
            }
            edtTextSend.setText("");
        });
        seenMessage(userid);
        fUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users").child(userid);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Users users = snapshot.getValue(Users.class);
                username.setText(users.getFullname());
                if (users.getImage().equals("default")) {
                    profile_image.setImageResource(R.mipmap.ic_launcher);
                } else {
                    Glide.with(getApplicationContext()).load(users.getImage()).into(profile_image);
                }
                readMessages(fUser.getUid(), userid, users.getImage());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        videoCall.setOnClickListener(v -> {
            Intent intent = new Intent(ChatActivity.this, OutGoingInvititationActivity.class);
            startActivity(intent);
        });

        audioCall.setOnClickListener(v -> {
            Intent intent = new Intent(ChatActivity.this, OutGoingInvititationActivity.class);
            startActivity(intent);
        });
    }
    private void seenMessage(String userid){
        reference = FirebaseDatabase.getInstance().getReference("Chats");
        seenListener = reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot: snapshot.getChildren()){
                    Chat chat = dataSnapshot.getValue(Chat.class);
                    if (chat.getReceiver().equals(fUser.getUid()) && chat.getSender().equals(userid)){
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("isSeen", true);
                        dataSnapshot.getRef().updateChildren(hashMap);

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void sendMessage(String sender, String receiver, String message) {
        reference = FirebaseDatabase.getInstance().getReference();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("sender", sender);
        hashMap.put("receiver", receiver);
        hashMap.put("message", message);
        hashMap.put("isSeen", false);

        reference.child("Chats").push().setValue(hashMap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Update the sender's last message and last message time
                reference.child("Users").child(sender).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        Users senderUser = snapshot.getValue(Users.class);
                        if (senderUser != null) {
                            senderUser.setLastMessage(message);
                            senderUser.setLastMessageTime(String.valueOf(System.currentTimeMillis()));
                            reference.child("Users").child(sender).setValue(senderUser);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

                // Update the receiver's last message and last message time
                reference.child("Users").child(receiver).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        Users receiverUser = snapshot.getValue(Users.class);
                        receiverUser.setLastMessage(message);
                        receiverUser.setLastMessageTime(String.valueOf(System.currentTimeMillis()));
                        reference.child("Users").child(receiver).setValue(receiverUser);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
    }

    private void readMessages(final String myId, final String userid, final String image) {
        mChat = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                mChat.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    Chat chats = dataSnapshot.getValue(Chat.class);
                    if (chats.getReceiver().equals(myId) && chats.getSender().equals(userid)
                            || chats.getReceiver().equals(userid) && chats.getSender().equals(myId)) {
                        mChat.add(chats);
                    }
                }
                // Sort the messages by time
                Collections.sort(mChat, new Comparator<Chat>() {
                    @Override
                    public int compare(Chat chat1, Chat chat2) {
                        Date date1 = chat1.getTime();
                        Date date2 = chat2.getTime();
                        return date1.compareTo(date2);
                    }
                });
                messageAdapter = new MessageAdapter(ChatActivity.this, mChat, image);
                recyclerView.setAdapter(messageAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    private  void status(String status){
        reference = FirebaseDatabase.getInstance().getReference("Users").child(fUser.getUid());
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("status", status);
        reference.updateChildren(hashMap);

    }


    @Override
    protected void onResume() {
        super.onResume();
        status("online");
    }

    @Override
    protected void onPause() {
        super.onPause();
        reference.removeEventListener(seenListener);
        status("offline");

    }

    @Override
    public void initateVideoMeeting(Users user) {
        if(user.getToken() == null || user.getToken().trim().isEmpty()){
            Toast.makeText(this,user.getFullname() + "is not online",Toast.LENGTH_SHORT).show();
        }else {
            Intent intent = new Intent(getApplicationContext(), OutGoingInvititationActivity.class);
            intent.putExtra("user",user);
            intent.putExtra("type","video");
            startActivity(intent);
        }
    }

    @Override
    public void initateAudioMeeting(Users user) {
        if(user.getToken() == null || user.getToken().trim().isEmpty()){
            Toast.makeText(this,user.getFullname() + "is not online",Toast.LENGTH_SHORT).show();
        }else {
            Intent intent = new Intent(getApplicationContext(), OutGoingInvititationActivity.class);
            intent.putExtra("user",user);
            intent.putExtra("type","audio");
            startActivity(intent);
        }
    }
}

